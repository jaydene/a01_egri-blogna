<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CS3140 project A01 for Jayden Egri-Blogna</title>
  <h1 style="text-align:left">Assignment 01 - Egri-Blogna, Jayden</h1>
</head>

<body>
<!--
  Summary:
  This section should include a basic biography and any interesting facts about
  yourself that you’d like to include. This should be in paragraph format.
-->

    <div>
      <h2>Summary: </h2>
      <p>
        My name is Jayden Egri-Blogna. I'm a 3rd-year student at BGSU, studying Computer Science. I am really interested to learn about Web Applications from this class.
      </p>
    </div>

<!--
  Personal Information:
  This should include name, address, phone number, email address.
-->
    <div>
      <h2>Personal Information: </h2>
      <p>
        Name: Jayden Egri-Blogna
        <br>
        Address: 706 Napoleon Rd, Bowling Green, OH 43402
        <br>
        Phone Number: (216)-375-4876
        <br>
        Email Address: jaydene@bgsu.edu
      </p>
    </div>

<!--
  Academic Information:
  List all of your high school and college information including any particular
  notes about classes that you have taken. You may include future education that
  you hope to have as well.
-->
    <div>
      <h2>Academic Information: </h2>
      <p>
        I attended North Royalton High School. I am currently enrolled at Bowling Green State University for my BS in Computer Science.
      </p>
    </div>

<!--
  Employment Information:
  List any employment past, present, or future. At least three.
-->
    <div>
      <h2>Employment Information: </h2>
      <ul>
        <li>Past Employment: Discount Drug Mart</li>
        <li>Present Employment: BGSU Regional Book Depository</li>
        <li>Future Employment: Progressive Insurance Company</li>
      </ul>
    </div>

</body>
</html>
